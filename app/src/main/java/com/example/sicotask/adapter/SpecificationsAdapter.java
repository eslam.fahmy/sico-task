package com.example.sicotask.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.databinding.RvTopCategoryItemBinding;
import com.example.sicotask.model.SpecificationsModel;
import com.example.sicotask.utils.CustomAnimation;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class SpecificationsAdapter extends RecyclerView.Adapter<SpecificationsAdapter.ViewHolder> {

    private Activity context;
    private List<SpecificationsModel> productModelList;
    private int animationScaleFactor = 100;


    public SpecificationsAdapter(Activity context, List<SpecificationsModel> productModelList) {
        this.context = context;
        this.productModelList = productModelList;

    }

    @NonNull
    @Override
    public SpecificationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvTopCategoryItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_top_category_item, parent, false);
        return new SpecificationsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SpecificationsAdapter.ViewHolder holder, final int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(productModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvTopCategoryItemBinding binding;

        ViewHolder(RvTopCategoryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;


        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

        void setViewModel(SpecificationsModel viewModel) {
            if (binding != null) {
                switch (viewModel.getId()) {
                    case 0:
                        binding.imgMakeUp.setBackground(context.getResources().getDrawable(R.drawable.circule_sky));
                        binding.imgMakeUp.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_camera_alt_black_24dp));
                        binding.tvMakeUp.setText(viewModel.getValue());
                        break;
                    case 1:
                        binding.imgMakeUp.setBackground(context.getResources().getDrawable(R.drawable.circule_orange));
                        binding.imgMakeUp.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_battery_charging_full_black_24dp));
                        binding.tvMakeUp.setText(viewModel.getValue());   break;
                    case 2:
                        binding.imgMakeUp.setBackground(context.getResources().getDrawable(R.drawable.circule_skin));
                        binding.imgMakeUp.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_sd_storage_black_24dp));
                        binding.tvMakeUp.setText(viewModel.getValue());  break;
                    case 3:
                        binding.imgMakeUp.setBackground(context.getResources().getDrawable(R.drawable.circule_hair));
                        binding.imgMakeUp.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_network_cell_black_24dp));
                        binding.tvMakeUp.setText(viewModel.getValue()); break;
                    case 4:
                        binding.imgMakeUp.setBackground(context.getResources().getDrawable(R.drawable.circule_blue));
                        //    binding.imgMakeUp.setImageDrawable(context.getResources().getDrawable(R.drawable.shaver));
                        //    binding.tvMakeUp.setText(context.getResources().getString(R.string.wax));
                        break;

                }
                CustomAnimation.scaleOutWithPulse(binding.imgMakeUp, animationScaleFactor, 300);
                CustomAnimation.scaleOutWithPulse(binding.tvMakeUp, animationScaleFactor, 300);
                animationScaleFactor = animationScaleFactor + 30;
            }
        }


    }

    @Override
    public void onViewAttachedToWindow(@NonNull SpecificationsAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull SpecificationsAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


}


