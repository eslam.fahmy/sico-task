package com.example.sicotask.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.databinding.RvProductItemBinding;
import com.example.sicotask.listener.ProductListener;
import com.example.sicotask.model.ProductModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Activity context;
    private List<ProductModel> productModelList;
    private ProductListener listener;

    public ProductAdapter(Activity context, List<ProductModel> productModelList, ProductListener listener) {
        this.context = context;
        this.productModelList = productModelList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RvProductItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_product_item, parent, false);
        return new ProductAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductAdapter.ViewHolder holder, final int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(productModelList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return productModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvProductItemBinding binding;

        ViewHolder(RvProductItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(ProductModel productModel, int position) {

            if (binding != null) {
                binding.setProduct(productModel);
                binding.setListener(listener);
                binding.setPosition(position);
                switch (getAdapterPosition() % 4) {
                    case 0:
                        binding.layoutContainer.setBackground(context.getResources().getDrawable(R.drawable.shadow_spa));
                        break;
                    case 1:
                        binding.layoutContainer.setBackground(context.getResources().getDrawable(R.drawable.shadow_nail));
                        break;
                    case 2:
                        binding.layoutContainer.setBackground(context.getResources().getDrawable(R.drawable.shadow_hair));
                        break;
                    case 3:
                        binding.layoutContainer.setBackground(context.getResources().getDrawable(R.drawable.shadow_wax));
                        break;
                }

            }
        }

    }

    @Override
    public void onViewAttachedToWindow(@NonNull ProductAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ProductAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


}


