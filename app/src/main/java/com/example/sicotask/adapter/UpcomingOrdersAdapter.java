package com.example.sicotask.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.cache.OrderConstant;
import com.example.sicotask.cache.model.RoomOrderObjectModel;
import com.example.sicotask.databinding.RvOrdersNotSubmitItemBinding;
import com.example.sicotask.databinding.RvOrdersSubmitedItemBinding;
import com.example.sicotask.listener.HistoryOrderListener;
import com.example.sicotask.listener.OrderListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class UpcomingOrdersAdapter extends RecyclerView.Adapter<UpcomingOrdersAdapter.ViewHolder> {

    private static final int SUBMITTED = 0;
    private static final int NOT_SUBMITTED = 1;
    private Activity context;
    private List<RoomOrderObjectModel> orderObjectModelList;
    private OrderListener listener;
    private HistoryOrderListener historyListener;


    public UpcomingOrdersAdapter(Activity context, List<RoomOrderObjectModel> orderObjectModelList, HistoryOrderListener listener) {
        this.context = context;
        this.orderObjectModelList = orderObjectModelList;
        this.historyListener = listener;

    }

    public UpcomingOrdersAdapter(Activity context, List<RoomOrderObjectModel> orderObjectModelList, OrderListener listener) {
        this.context = context;
        this.orderObjectModelList = orderObjectModelList;
        this.listener = listener;

    }

    public int getItemViewType(int position) {
        if (orderObjectModelList.get(position).isPast())
            return SUBMITTED;
        else
            return NOT_SUBMITTED;
    }


    @NonNull
    @Override
    public UpcomingOrdersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == NOT_SUBMITTED) {
            RvOrdersNotSubmitItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_orders_not_submit_item, parent, false);
            binding.setListner(listener);
            return new UpcomingOrdersAdapter.ViewHolder(binding, viewType);
        } else {
            RvOrdersSubmitedItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_orders_submited_item, parent, false);
            binding.setListener(historyListener);
            return new UpcomingOrdersAdapter.ViewHolder(binding, viewType);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull final UpcomingOrdersAdapter.ViewHolder holder, final int position) {
        if (position != RecyclerView.NO_POSITION)
            holder.setViewModel(orderObjectModelList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return orderObjectModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private RvOrdersNotSubmitItemBinding notSubmitItemBinding;
        private RvOrdersSubmitedItemBinding submitedItemBinding;
        private int viewType;

        ViewHolder(RvOrdersNotSubmitItemBinding binding, int viewType) {
            super(binding.getRoot());
            this.notSubmitItemBinding = binding;
            this.viewType = viewType;

        }

        ViewHolder(RvOrdersSubmitedItemBinding binding, int viewType) {
            super(binding.getRoot());
            this.submitedItemBinding = binding;
            this.viewType = viewType;
        }


        void bind() {

            switch (viewType) {
                case SUBMITTED:
                    if (submitedItemBinding != null) {
                        submitedItemBinding = DataBindingUtil.bind(itemView);
                    }
                    break;
                case NOT_SUBMITTED:
                    if (notSubmitItemBinding != null) {
                        notSubmitItemBinding = DataBindingUtil.bind(itemView);

                    }
                    break;
            }
        }

        void unbind() {
            switch (viewType) {
                case SUBMITTED:
                    if (submitedItemBinding != null) {
                        submitedItemBinding.unbind();
                    }
                    break;
                case NOT_SUBMITTED:
                    if (notSubmitItemBinding != null) {
                        notSubmitItemBinding.unbind();
                    }
                    break;
            }
        }


        void setViewModel(RoomOrderObjectModel roomOrderObjectModel, int position) {

            switch (viewType) {
                case SUBMITTED:
                    if (submitedItemBinding != null) {
                        submitedItemBinding.setProduct(roomOrderObjectModel);
                        switch (roomOrderObjectModel.getStatus()) {
                            case OrderConstant.CANCELLED:
                                submitedItemBinding.tvStatusValue.setText(context.getResources().getString(R.string.cancel));
                                break;
                            case OrderConstant.SUCCESS:
                                submitedItemBinding.tvStatusValue.setText(context.getResources().getString(R.string.success));
                                break;

                        }
                    }
                    break;
                case NOT_SUBMITTED:
                    if (notSubmitItemBinding != null) {
                        notSubmitItemBinding.setProduct(roomOrderObjectModel);

                    }
                    break;
            }
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull UpcomingOrdersAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull UpcomingOrdersAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }


}


