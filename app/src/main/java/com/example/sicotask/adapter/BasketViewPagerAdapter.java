package com.example.sicotask.adapter;

import android.content.Context;

import com.example.sicotask.R;
import com.example.sicotask.fragment.HistoryOrderFragment;
import com.example.sicotask.fragment.UpcomingOrdersFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class BasketViewPagerAdapter extends FragmentStatePagerAdapter {


    private static final int NUM_ITEMS = 2;
    private Context context;

    public BasketViewPagerAdapter(Context context, FragmentManager fragmentManager) {
        super(fragmentManager);
        this.context = context;
    }


    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return new UpcomingOrdersFragment();
            case 1: // Fragment # 0 - This will show FirstFragment different title

                return new HistoryOrderFragment();

        }
        return null;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return context.getString(R.string.up_coming_orders);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return context.getString(R.string.history);
        }
        return null;
    }

}


