package com.example.sicotask.activity;

import android.os.Bundle;

import com.example.sicotask.R;
import com.example.sicotask.fragment.ShoppingFragment;
import com.example.sicotask.listener.FBack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends AppCompatActivity  implements FragmentManager.OnBackStackChangedListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_main);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainContent, ShoppingFragment.newInstance())
                .addToBackStack("SignInFragment")
                .commitAllowingStateLoss();
    }


    @Override
    public void onBackStackChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.mainContent);
        if (fragment instanceof FBack)
            ((FBack) fragment).onFragmentUpdate();
    }
}
