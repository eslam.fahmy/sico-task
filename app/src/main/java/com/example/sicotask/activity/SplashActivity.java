package com.example.sicotask.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;

import com.example.sicotask.utils.Constant;
import com.example.sicotask.utils.UtilsPreference;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import io.kashier.sdk.Core.network.SDK_MODE;
import io.kashier.sdk.KASHIER_DISPLAY_LANG;
import io.kashier.sdk.Kashier;

public class SplashActivity extends AppCompatActivity {





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        updateLanguage();
        String lan = UtilsPreference.getInstance(this).getPreference(Constant.LANGUAGE, Resources.getSystem().getConfiguration().locale.getLanguage());
        if (lan.equals("en"))
            Kashier.init(this, Constant.merchantId, Constant.ApiKeyId, Constant.currency, SDK_MODE.DEVELOPMENT, KASHIER_DISPLAY_LANG.AR);
        else
            Kashier.init(this, Constant.merchantId, Constant.ApiKeyId, Constant.currency, SDK_MODE.DEVELOPMENT, KASHIER_DISPLAY_LANG.EN);


        if (mAuth.getCurrentUser() == null)
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        else
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        SplashActivity.this.finish();
    }

    private void updateLanguage() {

        String lan = UtilsPreference.getInstance(this).getPreference(Constant.LANGUAGE, Resources.getSystem().getConfiguration().locale.getLanguage());
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Resources resources = this.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale);
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());


    }
}
