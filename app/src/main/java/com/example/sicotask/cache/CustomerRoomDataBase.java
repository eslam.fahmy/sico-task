package com.example.sicotask.cache;


import android.content.Context;

import com.example.sicotask.cache.dao.FullOrderDao;
import com.example.sicotask.cache.model.RoomOrderObjectModel;
import com.example.sicotask.utils.Constant;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;


@Database(entities = {RoomOrderObjectModel.class}, version = 5, exportSchema = false)
public abstract class CustomerRoomDataBase extends RoomDatabase {


    private static CustomerRoomDataBase instance;


    public abstract FullOrderDao orderDao();


    static CustomerRoomDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    CustomerRoomDataBase.class, Constant.DATA_BASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    private static void onDestroyInstance() {
        instance = null;
    }

}
