package com.example.sicotask.cache.dao;


import com.example.sicotask.cache.model.RoomOrderObjectModel;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface FullOrderDao {


    @Insert(onConflict = REPLACE)
    void insert(RoomOrderObjectModel order);

    @Update
    void update(RoomOrderObjectModel order);

    @Delete
    void delete(RoomOrderObjectModel order);


    @Query("SELECT * FROM RoomOrderObjectModel")
    LiveData<List<RoomOrderObjectModel>> getAllOrders();

    @Query("SELECT * FROM RoomOrderObjectModel WHERE past = 0")
    LiveData<List<RoomOrderObjectModel>> getAllOrdersUpComing();

    @Query("SELECT * FROM RoomOrderObjectModel WHERE past = 1")
    LiveData<List<RoomOrderObjectModel>> getAllOrdersPast();

    @Query("DELETE FROM  RoomOrderObjectModel")
    void deleteAll();
}
