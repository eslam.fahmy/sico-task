package com.example.sicotask.cache.viewModel;

import android.app.Application;

import com.example.sicotask.cache.Repository;
import com.example.sicotask.cache.model.RoomOrderObjectModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class OrderViewModel extends AndroidViewModel {

    private Repository repository;
    private LiveData<List<RoomOrderObjectModel>> allOrders;
    private LiveData<List<RoomOrderObjectModel>> allPastOrders;

    public OrderViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
        allOrders = repository.getAllOrders();
        allPastOrders = repository.getAllPasstOrders();
    }

    public LiveData<List<RoomOrderObjectModel>> getAllOrders() {
        return allOrders;
    }

    public LiveData<List<RoomOrderObjectModel>> getAllPastOrders() {
        return allPastOrders;
    }
}
