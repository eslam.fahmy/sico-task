package com.example.sicotask.cache;

import android.app.Application;
import android.os.AsyncTask;

import com.example.sicotask.cache.dao.FullOrderDao;
import com.example.sicotask.cache.model.RoomOrderObjectModel;

import java.util.List;

import androidx.lifecycle.LiveData;

public class Repository {


    private FullOrderDao fullOrderDao;
    private LiveData<List<RoomOrderObjectModel>> allOrders;
    private LiveData<List<RoomOrderObjectModel>> allPastOrders;



    public Repository(Application application) {
        CustomerRoomDataBase database = CustomerRoomDataBase.getInstance(application);
        fullOrderDao = database.orderDao();
        allOrders = fullOrderDao.getAllOrdersUpComing();
        allPastOrders = fullOrderDao.getAllOrdersPast();


    }

    public LiveData<List<RoomOrderObjectModel>> getAllOrders() {
        return allOrders;
    }

    public LiveData<List<RoomOrderObjectModel>> getAllPasstOrders() {
        return allPastOrders;
    }

    public void inset(RoomOrderObjectModel orderObjectModel) {
        new InsertOrder(fullOrderDao, orderObjectModel).execute();
    }



    public void update(RoomOrderObjectModel order) {
        new UpDateOrder(fullOrderDao, order).execute();
    }

    public void deleteAllOrders() {
        new DeleteALlOrder(fullOrderDao).execute();
    }


    private static class DeleteALlOrder extends AsyncTask<Void, Void, Void> {

        private FullOrderDao dao;


        DeleteALlOrder(FullOrderDao fullOrderDao) {
            this.dao = fullOrderDao;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.deleteAll();
            return null;
        }
    }


    private static class InsertOrder extends AsyncTask<Void, Void, Void> {

        private FullOrderDao dao;
        private RoomOrderObjectModel model;

        InsertOrder(FullOrderDao fullOrderDao, RoomOrderObjectModel orderObjectModel) {
            this.dao = fullOrderDao;
            this.model = orderObjectModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.insert(model);
            return null;
        }
    }

    //---------------------------------------------------------------------------------------------

    private static class UpDateOrder extends AsyncTask<Void, Void, Void> {

        private FullOrderDao dao;
        private RoomOrderObjectModel model;

        UpDateOrder(FullOrderDao fullOrderDao, RoomOrderObjectModel orderObjectModel) {
            this.dao = fullOrderDao;
            this.model = orderObjectModel;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.update(model);
            return null;
        }
    }

}
