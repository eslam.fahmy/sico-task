package com.example.sicotask.fragment;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.sicotask.R;
import com.example.sicotask.adapter.BasketViewPagerAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;


public class BasketFragment extends Fragment {

    private ViewPager vpPager;
    private BasketViewPagerAdapter basketViewPagerAdapter;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_basket, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (getView() != null) {
            vpPager = getView().findViewById(R.id.vpPager);
            progressBar = getView().findViewById(R.id.progressBar);
            new BasketFragment.SetUpViewPagerTask().execute();
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class SetUpViewPagerTask extends AsyncTask<Void, Void, Void> {
        AppCompatActivity context;

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            context = (AppCompatActivity) getActivity();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            basketViewPagerAdapter = new BasketViewPagerAdapter(context, getChildFragmentManager());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            vpPager.setAdapter(basketViewPagerAdapter);
            vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    if (i == 1) {
                        basketViewPagerAdapter.notifyDataSetChanged();
                    }

                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("BASKET onResume ");
        if (basketViewPagerAdapter != null)
            basketViewPagerAdapter.notifyDataSetChanged();
    }


}
