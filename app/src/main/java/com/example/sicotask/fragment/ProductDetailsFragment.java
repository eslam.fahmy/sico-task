package com.example.sicotask.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.adapter.SpecificationsAdapter;
import com.example.sicotask.cache.OrderConstant;
import com.example.sicotask.cache.Repository;
import com.example.sicotask.cache.model.RoomOrderObjectModel;
import com.example.sicotask.databinding.FragmentProductDetailsBinding;
import com.example.sicotask.model.ProductModel;
import com.example.sicotask.utils.CustomAnimation;
import com.example.sicotask.utils.GlobalFunctions;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductDetailsFragment extends Fragment implements View.OnClickListener {

    private FragmentProductDetailsBinding binding;
    private ProductModel productModel;
    private int position;
    private int animationOffset = 0;
    private int quantity = 1;
    private float basePrice, finalPrice;

    private ProductDetailsFragment(ProductModel productModel, int position) {
        this.productModel = productModel;
        this.position = position;
    }

    static Fragment newInstance(ProductModel productModel, int position) {
        return new ProductDetailsFragment(productModel, position);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false);
        binding.setProduct(productModel);
        basePrice = Float.valueOf(productModel.getPrice());
        finalPrice = Float.valueOf(productModel.getPrice());
        switch (position % 4) {
            case 0:
                binding.img.setImageDrawable(ProductDetailsFragment.this.getResources().getDrawable(R.color.orange));
                GlobalFunctions.setStatusBarColor(getActivity(), R.color.orange);
                break;
            case 1:
                binding.img.setImageDrawable(ProductDetailsFragment.this.getResources().getDrawable(R.color.skin));
                GlobalFunctions.setStatusBarColor(getActivity(), R.color.skin);
                break;
            case 2:
                binding.img.setImageDrawable(ProductDetailsFragment.this.getResources().getDrawable(R.color.hair));
                GlobalFunctions.setStatusBarColor(getActivity(), R.color.hair);
                break;
            case 3:
                binding.img.setImageDrawable(ProductDetailsFragment.this.getResources().getDrawable(R.color.sky));
                GlobalFunctions.setStatusBarColor(getActivity(), R.color.sky);
                break;
        }
        SpecificationsAdapter purchaseAdapter = new SpecificationsAdapter(getActivity(), productModel.getSpecifications());
        binding.layoutCategories.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        binding.layoutCategories.setAdapter(purchaseAdapter);
        binding.imgBack.setOnClickListener(this);
        binding.btnBook.setOnClickListener(this);
        binding.imgIncrease.setOnClickListener(this);
        binding.imgDecrease.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvCamera, animationOffset);
        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvCameraDetails, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvDisplay, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvDisplayDetails, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvChipest, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvChipestDetails, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvMemory, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvMemoryDetails, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvOther, animationOffset);

        animationOffset += 100;
        CustomAnimation.slide(ProductDetailsFragment.this.getContext(), binding.tvOtherDetails, animationOffset);
    }

    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        switch (view.getId()) {
            case R.id.imgBack:
                if (getActivity() != null)
                    getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.btnBook:
                addToBasket();
                break;
            case R.id.imgIncrease:
                quantity++;
                finalPrice = quantity * basePrice;
                binding.tvTotalPrice.setText(String.valueOf(finalPrice));
                binding.tvQanitiy.setText(String.valueOf(quantity));
                break;
            case R.id.imgDecrease:
                if (quantity > 1) {
                    quantity--;
                    finalPrice = quantity * basePrice;
                    binding.tvTotalPrice.setText(String.valueOf(finalPrice));
                    binding.tvQanitiy.setText(String.valueOf(quantity));
                }
        }
    }

    private void addToBasket() {
        if (ProductDetailsFragment.this.getActivity() == null)
            return;
        Repository repository = new Repository(ProductDetailsFragment.this.getActivity().getApplication());
        RoomOrderObjectModel orderObjectModel = new RoomOrderObjectModel();
        orderObjectModel.setName(productModel.getName());
        orderObjectModel.setPrice(String.valueOf(finalPrice));
        orderObjectModel.setPast(false);
        orderObjectModel.setQuantity(quantity);
        orderObjectModel.setStatus(OrderConstant.NEW);
        repository.inset(orderObjectModel);
        ProductDetailsFragment.this.getActivity().getSupportFragmentManager().popBackStack();
    }
}
