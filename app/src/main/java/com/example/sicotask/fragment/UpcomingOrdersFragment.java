package com.example.sicotask.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sicotask.R;
import com.example.sicotask.adapter.UpcomingOrdersAdapter;
import com.example.sicotask.cache.OrderConstant;
import com.example.sicotask.cache.Repository;
import com.example.sicotask.cache.model.RoomOrderObjectModel;
import com.example.sicotask.cache.viewModel.OrderViewModel;
import com.example.sicotask.databinding.FragmentUpcmoingBinding;
import com.example.sicotask.listener.OrderListener;
import com.example.sicotask.utils.Constant;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import io.kashier.sdk.Core.model.Response.Error.ErrorData;
import io.kashier.sdk.Core.model.Response.Payment.PaymentResponse;
import io.kashier.sdk.Core.model.Response.UserCallback;
import io.kashier.sdk.Kashier;
import io.kashier.sdk.UserDailogUtils.PaymentActivityConfig;
import retrofit2.Response;

public class UpcomingOrdersFragment extends Fragment implements OrderListener {


    private FragmentUpcmoingBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_upcmoing, container, false);
        OrderViewModel viewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        viewModel.getAllOrders().observe(this, new Observer<List<RoomOrderObjectModel>>() {
            @Override
            public void onChanged(List<RoomOrderObjectModel> roomOrderObjectModels) {

                if (roomOrderObjectModels == null || roomOrderObjectModels.size() == 0) {
                    binding.layoutEmptyBasket.setVisibility(View.VISIBLE);


                    binding.layoutEmptyBasket.findViewById(R.id.btnBrowse).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getActivity() != null)
                                getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                    return;
                }


                binding.layoutEmptyBasket.setVisibility(View.GONE);
                UpcomingOrdersAdapter ordersAdapter = new UpcomingOrdersAdapter(getActivity(), roomOrderObjectModels, UpcomingOrdersFragment.this);
                binding.rvOrder.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.rvOrder.setAdapter(ordersAdapter);


            }
        });
        return binding.getRoot();
    }


    @Override
    public void onPayClicked(final RoomOrderObjectModel order) {

        if (UpcomingOrdersFragment.this.getActivity() == null)
            return;

        Kashier.startPaymentActivity(
                (AppCompatActivity) UpcomingOrdersFragment.this.getActivity(),
                Constant.SHOPPER,
                String.valueOf(order.getId()),
                order.getPrice(),

                new UserCallback<PaymentResponse>() {
                    @Override
                    public void onResponse(final Response<PaymentResponse> userResponse) {
                        if (UpcomingOrdersFragment.this.getActivity() == null)
                            return;

                        if (!isAdded())
                            return;
                        if (userResponse == null || userResponse.body() == null)
                            return;
                        if (userResponse.body().getStatus().equals(Constant.SUCCESS)) {


                            if (UpcomingOrdersFragment.this.getActivity() == null)
                                return;
                            UpcomingOrdersFragment.this.getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (UpcomingOrdersFragment.this.getActivity().getResources().getBoolean(R.bool.isEnglish)) {
                                        Toast.makeText(UpcomingOrdersFragment.this.getActivity(), userResponse.body().getMessages().getEn(), Toast.LENGTH_SHORT).show();
                                    } else
                                        Toast.makeText(UpcomingOrdersFragment.this.getActivity(), userResponse.body().getMessages().getAr(), Toast.LENGTH_SHORT).show();

                                }
                            });


                            if (UpcomingOrdersFragment.this.getActivity() == null)
                                return;
                            Repository repository = new Repository(UpcomingOrdersFragment.this.getActivity().getApplication());

                            order.setPast(true);
                            order.setStatus(OrderConstant.SUCCESS);
                            repository.update(order);

                        }


                    }

                    @Override
                    public void onFailure(final ErrorData<PaymentResponse> errorData) {
                        if (UpcomingOrdersFragment.this.getActivity() == null)
                            return;
                        UpcomingOrdersFragment.this.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(UpcomingOrdersFragment.this.getActivity(), errorData.getErrorMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                }, new PaymentActivityConfig()
                        .

                                setActivityTitle(UpcomingOrdersFragment.this.getActivity().

                                        getResources().

                                        getString(R.string.kashier))
                        .

                                setButtonLabel(UpcomingOrdersFragment.this.getResources().

                                        getString(R.string.pay_now) + " %2$s %1$s"));

    }

    @Override
    public void onCancel(RoomOrderObjectModel order) {

        if (getActivity() == null)
            return;
        Repository repository = new Repository(getActivity().getApplication());

        order.setPast(true);
        order.setStatus(OrderConstant.CANCELLED);
        repository.update(order);


    }
}
