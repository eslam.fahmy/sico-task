package com.example.sicotask.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.example.sicotask.R;
import com.example.sicotask.adapter.ProductAdapter;
import com.example.sicotask.cache.Repository;
import com.example.sicotask.cache.model.RoomOrderObjectModel;
import com.example.sicotask.databinding.ShoppingFragmentBinding;
import com.example.sicotask.listener.FBack;
import com.example.sicotask.listener.ProductListener;
import com.example.sicotask.model.ProductModel;
import com.example.sicotask.utils.GlobalFunctions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

public class ShoppingFragment extends Fragment implements ProductListener, FBack, View.OnClickListener {


    private ShoppingFragmentBinding binding;
    private List<ProductModel> productModels;

    public static Fragment newInstance() {
        return new ShoppingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.shopping_fragment, container, false);
        productModels = GlobalFunctions.populateDate();
        binding.imgBasket.setOnClickListener(this);
        binding.imgUser.setOnClickListener(this);
        initListeners();
        return binding.getRoot();
    }

    private void initListeners() {

        if (ShoppingFragment.this.getActivity() == null)
            return;
        Repository repository = new Repository(ShoppingFragment.this.getActivity().getApplication());
        repository.getAllOrders().observe(this, new Observer<List<RoomOrderObjectModel>>() {
            @Override
            public void onChanged(List<RoomOrderObjectModel> roomOrderObjectModels) {
                if (roomOrderObjectModels != null && roomOrderObjectModels.size() > 0) {
                    binding.tvOrderCount.setVisibility(View.VISIBLE);
                    binding.tvOrderCount.setText(("" + roomOrderObjectModels.size()));
                } else
                    binding.tvOrderCount.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(ShoppingFragment.this.getContext(), R.anim.layout_animation_fall_down);
        final Animation slide = AnimationUtils.loadAnimation(ShoppingFragment.this.getContext(), R.anim.item_animation_fall_down);
        binding.tvTitle.setAnimation(slide);
        binding.imgBasket.setAnimation(slide);
        ProductAdapter purchaseAdapter = new ProductAdapter(getActivity(), productModels, ShoppingFragment.this);
        binding.rvProducts.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvProducts.setAdapter(purchaseAdapter);
        binding.rvProducts.setLayoutAnimation(controller);
    }

    @Override
    public void onProductClicked(ProductModel productModel, int position) {

        if (getActivity() != null)
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_out_down, R.anim.slide_out_down)
                    .add(R.id.mainContent, ProductDetailsFragment.newInstance(productModel, position))
                    .addToBackStack("fragment")
                    .commit();


    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null)
            getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onFragmentUpdate() {
        GlobalFunctions.setStatusBarColor(getActivity(), R.color.white);
    }

    @Override
    public void onClick(View view) {
        if (view == null)
            return;

        switch (view.getId()) {
            case R.id.imgBasket:
                if (ShoppingFragment.this.getActivity() != null)
                    ShoppingFragment.this.getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .replace(R.id.mainContent, new BasketFragment())
                            .addToBackStack("BasketFragment")
                            .commit();

                break;
            case R.id.imgUser:

                if (ShoppingFragment.this.getActivity() != null)
                    ShoppingFragment.this.getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .replace(R.id.mainContent, new UserFragment())
                            .addToBackStack("UserFragment")
                            .commit();

                break;
        }


    }
}
