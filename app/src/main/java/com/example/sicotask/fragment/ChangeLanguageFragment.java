package com.example.sicotask.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.activity.SplashActivity;
import com.example.sicotask.databinding.FragmentChangeLanguageBinding;
import com.example.sicotask.utils.Constant;
import com.example.sicotask.utils.UtilsPreference;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class ChangeLanguageFragment extends Fragment implements View.OnClickListener {

    private String lan = null;
    private FragmentChangeLanguageBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_language, container, false);
        boolean lang = getResources().getBoolean(R.bool.isEnglish);
        if (lang) {
            lan = "en";
            binding.cbEnglsih.setChecked(true);
            binding.cbArabic.setChecked(false);
        } else {
            lan = "ar";
            binding.cbEnglsih.setChecked(false);
            binding.cbArabic.setChecked(true);
        }
        binding.imgBack.setOnClickListener(this);
        binding.layoutEnglishLayout.setOnClickListener(this);
        binding.layoutArabicLayout.setOnClickListener(this);
        binding.btnConfirm.setOnClickListener(this);
        return binding.getRoot();

    }


    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        switch (view.getId()) {
            case R.id.imgBack:
                if (ChangeLanguageFragment.this.getActivity() != null)
                    ChangeLanguageFragment.this.getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.layoutEnglishLayout:
                lan = "en";
                binding.cbEnglsih.setChecked(true);
                binding.cbArabic.setChecked(false);
                break;
            case R.id.layoutArabicLayout:
                lan = "ar";
                binding.cbEnglsih.setChecked(false);
                binding.cbArabic.setChecked(true);
                break;
            case R.id.btnConfirm:

                if (getActivity() == null)
                    return;
                if (lan == null)
                    return;


                UtilsPreference.getInstance(getActivity()).savePreference(Constant.LANGUAGE, lan);
                Locale locale = new Locale(lan);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());

                Intent refresh = new Intent(getActivity(), SplashActivity.class);
                startActivity(refresh);
                getActivity().finish();


                break;
        }
    }
}
