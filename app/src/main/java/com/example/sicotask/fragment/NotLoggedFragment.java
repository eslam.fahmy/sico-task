package com.example.sicotask.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.databinding.FragmentNotLogedBinding;
import com.example.sicotask.utils.Constant;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class NotLoggedFragment extends Fragment implements View.OnClickListener {


    private long mLastClickTime = System.currentTimeMillis();


    public static Fragment newInstance() {
        return new NotLoggedFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentNotLogedBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_not_loged, container, false);
        binding.btnSignIn.setOnClickListener(this);
        binding.btnSignUp.setOnClickListener(this);
        binding.layoutChangeLanguage.setOnClickListener(this);
        return binding.getRoot();

    }

    @Override
    public void onClick(View v) {
        if (v == null)
            return;

        int id = v.getId();

        long now = System.currentTimeMillis();
        if (now - mLastClickTime < Constant.CLICK_TIME_INTERVAL) {
            return;
        }
        mLastClickTime = now;


        switch (id) {
            case R.id.btnSignIn:


                if (getActivity() != null) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.mainContent, SignInFragment.newInstance())
                            .addToBackStack("LegalConsentFragment")
                            .commit();
                }

                break;
            case R.id.btnSignUp:

                if (getActivity() != null) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.mainContent, SignUpFragment.newInstance())
                            .addToBackStack("LegalConsentFragment")
                            .commit();
                }

                break;

            case R.id.layoutChangeLanguage:

                /*
                if (getActivity() != null) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_out_down, R.anim.slide_out_down)
                            .add(R.id.mainContent, ChangeLanguageFragment.newInstance())
                            .addToBackStack("fragment")
                            .commit();
                }
                */
                break;
        }

    }
}
