package com.example.sicotask.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sicotask.R;
import com.example.sicotask.activity.MainActivity;
import com.example.sicotask.databinding.FragmentSignInBinding;
import com.example.sicotask.utils.GlobalFunctions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class SignInFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "SignInFragment";
    private FirebaseAuth mAuth;
    private FragmentSignInBinding binding;

    public static Fragment newInstance() {
        return new SignInFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false);
        binding.btnLogin.setOnClickListener(this);
        binding.tvRegister.setOnClickListener(this);
        binding.tbChangeLang.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if (v == null)
            return;

        int id = v.getId();


        switch (id) {
            case R.id.tvRegister:
                if (getActivity() != null) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.exit_to_right, R.anim.exit_to_right)
                            .add(R.id.mainContent, SignUpFragment.newInstance())
                            .addToBackStack("LegalConsentFragment")
                            .commit();
                }

                break;

            case R.id.btnLogin:
                login();
                break;

            case R.id.tbChangeLang:

                if (SignInFragment.this.getActivity() != null)
                    SignInFragment.this.getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_out_down, R.anim.slide_out_down)
                            .add(R.id.mainContent, new ChangeLanguageFragment())
                            .addToBackStack("ChangeLanguageFragment")
                            .commit();

                break;
        }

    }

    private void login() {

        if (SignInFragment.this.getActivity() == null)
            return;


        final String email = binding.etEmail.getText() != null ? binding.etEmail.getText().toString().trim() : "";
        if (email.isEmpty()) {
            binding.etEmail.setError(getResources().getString(R.string.emailRequired));
            binding.etEmail.requestFocus();
            return;
        }

        if (!GlobalFunctions.isEmailValid(email)) {
            binding.etEmail.setError(getString(R.string.valid_email_req));
            binding.etEmail.requestFocus();
            return;
        }

        final String password = binding.etPassWord.getText() != null ? binding.etPassWord.getText().toString().trim() : "";
        if (password.isEmpty()) {
            binding.etPassWord.setError(getResources().getString(R.string.passWordReuired));
            binding.etPassWord.requestFocus();
            return;
        }

        if (password.length() < 6) {
            binding.etPassWord.setError(getResources().getString(R.string.passWord));
            binding.etPassWord.requestFocus();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(SignInFragment.this.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.d(TAG, "signInWithEmail:success");
                            SignInFragment.this.getActivity().startActivity(new Intent(SignInFragment.this.getActivity(), MainActivity.class));
                            SignInFragment.this.getActivity().finish();

                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(SignInFragment.this.getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();

                        }

                    }
                });

    }
}
