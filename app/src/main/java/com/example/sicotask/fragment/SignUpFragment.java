package com.example.sicotask.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sicotask.R;
import com.example.sicotask.activity.MainActivity;
import com.example.sicotask.databinding.FragmentSignUpBinding;
import com.example.sicotask.utils.GlobalFunctions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "SignUpFragment";
    private FirebaseAuth mAuth;
    private FragmentSignUpBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);
        mAuth = FirebaseAuth.getInstance();
        binding.btnRegister.setOnClickListener(this);
        binding.tvLogin.setOnClickListener(this);
        binding.tbChangeLang.setOnClickListener(this);
        return binding.getRoot();
    }

    static Fragment newInstance() {
        return new SignUpFragment();
    }


    private void createUser() {


        if (SignUpFragment.this.getActivity() == null)
            return;

        final String email = binding.etEmail.getText() != null ? binding.etEmail.getText().toString().trim() : "";
        if (email.isEmpty()) {
            binding.etEmail.setError(getResources().getString(R.string.emailRequired));
            binding.etEmail.requestFocus();
            return;
        }

        if (!GlobalFunctions.isEmailValid(email)) {
            binding.etEmail.setError(getString(R.string.valid_email_req));
            binding.etEmail.requestFocus();
            return;
        }

        final String password = binding.etPassWord.getText() != null ? binding.etPassWord.getText().toString().trim() : "";
        if (password.isEmpty()) {
            binding.etPassWord.setError(getResources().getString(R.string.passWordReuired));
            binding.etPassWord.requestFocus();
            return;
        }

        if (password.length() < 6) {
            binding.etPassWord.setError(getResources().getString(R.string.passWord));
            binding.etPassWord.requestFocus();
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(SignUpFragment.this.getActivity());
        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(SignUpFragment.this.getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    progressDialog.dismiss();
                    Log.d(TAG, "createUserWithEmail:success");
                    Toast.makeText(SignUpFragment.this.getActivity(), SignUpFragment.this.getActivity().getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

                    SignUpFragment.this.getActivity().startActivity(new Intent(SignUpFragment.this.getActivity(), MainActivity.class));
                    SignUpFragment.this.getActivity().finish();

                } else {
                    progressDialog.dismiss();
                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                    Toast.makeText(SignUpFragment.this.getActivity(), SignUpFragment.this.getActivity().getResources().getString(R.string.sorry), Toast.LENGTH_SHORT).show();

                }

            }
        });

    }

    @Override
    public void onClick(View view) {

        if (view == null)
            return;
        switch (view.getId()) {
            case R.id.btnRegister:
                createUser();
                break;
            case R.id.tvLogin:
                if (getActivity() != null) {
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
                            .add(R.id.mainContent, SignInFragment.newInstance())
                            .addToBackStack("LegalConsentFragment")
                            .commit();
                }
                break;

            case R.id.tbChangeLang:

                if (SignUpFragment.this.getActivity() != null)
                    SignUpFragment.this.getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_out_down, R.anim.slide_out_down)
                            .add(R.id.mainContent, new ChangeLanguageFragment())
                            .addToBackStack("ChangeLanguageFragment")
                            .commit();

                break;

        }
    }
}
