package com.example.sicotask.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.activity.SplashActivity;
import com.example.sicotask.cache.Repository;
import com.example.sicotask.databinding.FragmentUserBinding;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class UserFragment extends Fragment implements View.OnClickListener {

    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        FragmentUserBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user, container, false);
        binding.setUser(mAuth.getCurrentUser());
        binding.imgBack.setOnClickListener(this);
        binding.tvLogOut.setOnClickListener(this);
        binding.tbChangeLang.setOnClickListener(this);
        return binding.getRoot();

    }

    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        switch (view.getId()) {
            case R.id.imgBack:
                if (UserFragment.this.getActivity() != null)
                    UserFragment.this.getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.tvLogOut:
                if (UserFragment.this.getActivity() == null)
                    return;
                Repository repository = new Repository(UserFragment.this.getActivity().getApplication());
                repository.deleteAllOrders();
                mAuth.signOut();
                UserFragment.this.getActivity().startActivity(new Intent(UserFragment.this.getActivity(), SplashActivity.class));
                UserFragment.this.getActivity().finish();
                break;
            case R.id.tbChangeLang:

                if (UserFragment.this.getActivity() != null)
                    UserFragment.this.getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_out_down, R.anim.slide_out_down)
                            .add(R.id.mainContent, new ChangeLanguageFragment())
                            .addToBackStack("ChangeLanguageFragment")
                            .commit();

                break;
        }
    }
}
