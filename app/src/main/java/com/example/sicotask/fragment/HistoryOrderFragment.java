package com.example.sicotask.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sicotask.R;
import com.example.sicotask.adapter.UpcomingOrdersAdapter;
import com.example.sicotask.cache.OrderConstant;
import com.example.sicotask.cache.Repository;
import com.example.sicotask.cache.model.RoomOrderObjectModel;
import com.example.sicotask.cache.viewModel.OrderViewModel;
import com.example.sicotask.databinding.FragmentUpcmoingBinding;
import com.example.sicotask.listener.HistoryOrderListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

public class HistoryOrderFragment extends Fragment implements HistoryOrderListener {

    private static final String TAG = "UpcomingOrdersFragment";
    private FragmentUpcmoingBinding binding;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_upcmoing, container, false);
        OrderViewModel viewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        viewModel.getAllPastOrders().observe(this, new Observer<List<RoomOrderObjectModel>>() {
            @Override
            public void onChanged(List<RoomOrderObjectModel> roomOrderObjectModels) {

                if (roomOrderObjectModels == null || roomOrderObjectModels.size() == 0) {
                    binding.layoutEmptyBasket.setVisibility(View.VISIBLE);


                    binding.layoutEmptyBasket.findViewById(R.id.btnBrowse).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getActivity() != null)
                                getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                    return;
                }


                binding.layoutEmptyBasket.setVisibility(View.GONE);
                UpcomingOrdersAdapter ordersAdapter = new UpcomingOrdersAdapter(getActivity(), roomOrderObjectModels, HistoryOrderFragment.this);
                binding.rvOrder.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.rvOrder.setAdapter(ordersAdapter);


            }
        });
        return binding.getRoot();
    }


    @Override
    public void onReorder(RoomOrderObjectModel order) {

        if (getActivity() == null)
            return;
        Repository repository = new Repository(getActivity().getApplication());


        RoomOrderObjectModel newOrder = new RoomOrderObjectModel();
        newOrder.setPast(false);
        newOrder.setStatus(OrderConstant.NEW);
        newOrder.setName(order.getName());
        newOrder.setPrice(order.getPrice());
        newOrder.setQuantity(order.getQuantity());

        repository.inset(newOrder);


    }
}
