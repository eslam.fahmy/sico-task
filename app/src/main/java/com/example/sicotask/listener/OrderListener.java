package com.example.sicotask.listener;

import com.example.sicotask.cache.model.RoomOrderObjectModel;

public interface OrderListener {

    void onPayClicked(RoomOrderObjectModel order);

    void onCancel(RoomOrderObjectModel order);



}
