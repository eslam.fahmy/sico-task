package com.example.sicotask.listener;

import com.example.sicotask.cache.model.RoomOrderObjectModel;

public interface HistoryOrderListener {

    void onReorder(RoomOrderObjectModel order);
}
