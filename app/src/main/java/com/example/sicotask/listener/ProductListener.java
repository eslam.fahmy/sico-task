package com.example.sicotask.listener;

import com.example.sicotask.model.ProductModel;

public interface ProductListener {

    void onProductClicked (ProductModel productModel , int position);
}
