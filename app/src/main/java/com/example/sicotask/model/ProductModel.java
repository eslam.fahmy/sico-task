package com.example.sicotask.model;

import java.util.ArrayList;
import java.util.List;

public class ProductModel {

    private String name;
    private List<SpecificationsModel> specifications;
    private SpecificationsModel camera;
    private SpecificationsModel display;
    private SpecificationsModel chipset;
    private SpecificationsModel memory;
    private SpecificationsModel other;
    private String price;

    public ProductModel(String name) {
        this.name = name;
        this.price = "4200";
        specifications = new ArrayList<>();
        SpecificationsModel model = new SpecificationsModel();
        model.setId(0);
        model.setValue("13 MP");
        specifications.add(model);

        SpecificationsModel model1 = new SpecificationsModel();
        model1.setId(1);
        model1.setValue("2800 mah");
        specifications.add(model1);

        SpecificationsModel mode2 = new SpecificationsModel();
        mode2.setId(2);
        mode2.setValue("4 GB");
        specifications.add(mode2);

        SpecificationsModel mode3 = new SpecificationsModel();
        mode3.setId(3);
        mode3.setValue("4G");
        specifications.add(mode3);


        camera = new SpecificationsModel();
        camera.setId(10);
        camera.setValue("Rear Camera: 13MP+ 5MPAF\n" +
                "Front Camera: 8MP AF");

        display = new SpecificationsModel();
        display.setId(11);
        display.setValue("Display Size: 5.7 Inch\n" +
                "Display Type: IPS 18:9 full screen\n" +
                "Display Resolution: HD+ 1440x720 Pixels");

        chipset = new SpecificationsModel();
        chipset.setId(12);
        chipset.setValue("CPU: MTK6750T Octa core 1.51 Ghz\n" +
                "GPU: Mali-T860\n" +
                "Operating System: Android 7");

        memory = new SpecificationsModel();
        memory.setId(13);
        memory.setValue("Internal Memory: 64GB expandable up to 64GB with SD Card\n" +
                "RAM: 4GB");

        other = new SpecificationsModel();
        other.setId(14);
        other.setValue("Battery: 2800mAh\n" +
                "SIM: Dual-Sim\n" +
                "Dimensions: 154.5x73x8.6 mm\n" +
                "Weight: 165g\n" +
                "Features: Fingerprint, Fast Charging");


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SpecificationsModel> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(List<SpecificationsModel> specifications) {
        this.specifications = specifications;
    }

    public SpecificationsModel getCamera() {
        return camera;
    }

    public void setCamera(SpecificationsModel camera) {
        this.camera = camera;
    }

    public SpecificationsModel getDisplay() {
        return display;
    }

    public void setDisplay(SpecificationsModel display) {
        this.display = display;
    }

    public SpecificationsModel getChipset() {
        return chipset;
    }

    public void setChipset(SpecificationsModel chipset) {
        this.chipset = chipset;
    }

    public SpecificationsModel getMemory() {
        return memory;
    }

    public void setMemory(SpecificationsModel memory) {
        this.memory = memory;
    }

    public SpecificationsModel getOther() {
        return other;
    }

    public void setOther(SpecificationsModel other) {
        this.other = other;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

