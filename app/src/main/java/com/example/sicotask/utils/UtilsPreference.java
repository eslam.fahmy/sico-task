package com.example.sicotask.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import static android.content.Context.MODE_PRIVATE;

public class UtilsPreference {


    private SharedPreferences sharedPreferences;

    @NonNull
    public static UtilsPreference getInstance(Context context) {
        return new UtilsPreference(context);
    }

    private UtilsPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getApplicationContext().getPackageName(), MODE_PRIVATE);
    }


    // save Data ..
    public void savePreference(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void savePreference(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void savePreference(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    // get Data ..
    public String getPreference(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public int getPreference(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public boolean getPreference(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    /*
    public void saveOrder(SocketOrderModel order) {

        try {
            savePreference("ORDER_ID", order.getOrderId());
            savePreference("ORDER_TRACK_NUMBRR", order.getTrackNum());
            savePreference("ORDER_NUM", order.getOrderNum());
            savePreference("ORDER_SUB_TOTAL", order.getSubtotal());
            savePreference("ORDER_TAX", order.getTax());
            savePreference("ORDER_VAT", order.getVat());
            savePreference("ORDER_DELIVERY", order.getDelivery());
            savePreference("ORDER_STATUS", order.getStatus());


            savePreference("ORDER_DISCOUNT", order.getDiscount());
            savePreference("ORDER_COUPON", order.getCoupon());
            savePreference("ORDER_ORDER_TIME", order.getOrderTime());
            savePreference("ORDER_ARRIVAL_TIME", order.getArrivalTime());

            savePreference("ORDER_FROM_NAME", order.getFrom().getName());
            savePreference("ORDER_FROM_NAME_EN", order.getFrom().getNameEn());
            savePreference("ORDER_FROM_PHONE", order.getFrom().getPhone());
            savePreference("ORDER_FROM_LAT", order.getFrom().getLat());
            savePreference("ORDER_FROM_LNG", order.getFrom().getLng());
            savePreference("ORDER_FROM_ADDRESS", order.getFrom().getAddress());
            savePreference("ORDER_FROM_ADDRESS_EN", order.getFrom().getAddressEn());
            savePreference("ORDER_FROM_IMAGE_URL", order.getFrom().getImgUrl());

            savePreference("ORDER_TO_NAME", order.getTo().getName());
            savePreference("ORDER_TO_PHONE", order.getTo().getPhone());
            savePreference("ORDER_TO_LAT", order.getTo().getLat());
            savePreference("ORDER_TO_LNG", order.getTo().getLng());
            savePreference("ORDER_TO_ADDRESS", order.getTo().getAddress());
            savePreference("ORDER_TOTAL", order.getTotal());
        } catch (Exception ignored) {
        }

    }

    public void deleteOrder() {

        savePreference("ORDER_ID", null);
        savePreference("ORDER_TRACK_NUMBRR", null);
        savePreference("ORDER_NUM", null);
        savePreference("ORDER_SUB_TOTAL", null);
        savePreference("ORDER_TAX", null);
        savePreference("ORDER_VAT", null);
        savePreference("ORDER_DELIVERY", null);
        savePreference("ORDER_STATUS", null);

        savePreference("ORDER_DISCOUNT", null);
        savePreference("ORDER_COUPON", null);
        savePreference("ORDER_ORDER_TIME", null);
        savePreference("ORDER_ARRIVAL_TIME", null);


        savePreference("ORDER_FROM_NAME", null);
        savePreference("ORDER_FROM_NAME_EN", null);
        savePreference("ORDER_FROM_PHONE", null);
        savePreference("ORDER_FROM_LAT", null);
        savePreference("ORDER_FROM_LNG", null);
        savePreference("ORDER_FROM_ADDRESS", null);
        savePreference("ORDER_FROM_ADDRESS_EN", null);
        savePreference("ORDER_FROM_IMAGE_URL", null);

        savePreference("ORDER_TO_NAME", null);
        savePreference("ORDER_TO_PHONE", null);
        savePreference("ORDER_TO_LAT", null);
        savePreference("ORDER_TO_LNG", null);
        savePreference("ORDER_TO_ADDRESS", null);
        savePreference("ORDER_TOTAL", null);


    }

    public SocketOrderModel getOrder() {
        SocketOrderModel order = new SocketOrderModel();

        order.setOrderId(getPreference("ORDER_ID", ""));
        order.setTrackNum(getPreference("ORDER_TRACK_NUMBRR", ""));
        order.setOrderNum(getPreference("ORDER_NUM", ""));

        order.setSubtotal(getPreference("ORDER_SUB_TOTAL", ""));
        order.setTax(getPreference("ORDER_TAX", ""));
        order.setVat(getPreference("ORDER_VAT", ""));
        order.setDelivery(getPreference("ORDER_DELIVERY", ""));
        order.setStatus(getPreference("ORDER_STATUS", ""));

        order.setDiscount(getPreference("ORDER_DISCOUNT", ""));
        order.setCoupon(getPreference("ORDER_COUPON", ""));
        order.setOrderTime(getPreference("ORDER_ORDER_TIME", ""));
        order.setArrivalTime(getPreference("ORDER_ARRIVAL_TIME", ""));
        order.setTotal(getPreference("ORDER_TOTAL", ""));


        SocketOrderFrom form = new SocketOrderFrom();

        form.setName(getPreference("ORDER_FROM_NAME", ""));
        form.setNameEn(getPreference("ORDER_FROM_NAME_EN", ""));
        form.setPhone(getPreference("ORDER_FROM_PHONE", ""));
        form.setLat(getPreference("ORDER_FROM_LAT", ""));
        form.setLng(getPreference("ORDER_FROM_LNG", ""));
        form.setAddress(getPreference("ORDER_FROM_ADDRESS", ""));
        form.setAddressEn(getPreference("ORDER_FROM_ADDRESS_EN", ""));
        form.setImgUrl(getPreference("ORDER_FROM_IMAGE_URL", ""));

        order.setFrom(form);

        SocketOrderTo to = new SocketOrderTo();


        to.setName(getPreference("ORDER_TO_NAME", ""));
        to.setPhone(getPreference("ORDER_TO_PHONE", ""));
        to.setLat(getPreference("ORDER_TO_LAT", ""));
        to.setLng(getPreference("ORDER_TO_LNG", ""));
        to.setAddress(getPreference("ORDER_TO_ADDRESS", ""));

        order.setTo(to);

        return order;


    }

    public void saveLastState(String active) {
        savePreference(Constant.LAST_STATUS, active);
    }

    public String latStatus() {
        return getPreference(Constant.LAST_STATUS, null);
    }

    public void saveCaptain(ModelCaptain captain) {

        savePreference(Constant.DRIVER_ID, captain.getId());
        savePreference(Constant.DRIVER_NAME, captain.getName());
        savePreference(Constant.DRIVER_PHONE, captain.getPhone());
        savePreference(Constant.REFRESH_TOKEN, captain.getRefreshToken());


        savePreference(Constant.DRIVER_TOKEN, captain.getToken());
        savePreference(Constant.DRIVER_DEVICE_ID, captain.getDeviceID());

        savePreference(Constant.DRIVER_CITY_ID, captain.getCity().getId());
        savePreference(Constant.DRIVER_CITY_NAME, captain.getCity().getName());
        savePreference(Constant.DRIVER_EMAIL, captain.getEmail());

        savePreference(Constant.DRIVER_REAL_TIME_TOKEN, captain.getRealTimeToken());

        savePreference(Constant.DRIVER_IMAGE, captain.getImage());
        savePreference(Constant.DRIVER_NUMBER_OF_ORDERS, captain.getNumberOFOrders());
        savePreference(Constant.DRIVER_APPROVED, captain.isApproved());
        savePreference(Constant.DRIVER_LOCAL_IMAGE, captain.getLocalImage());


    }

    public void logout() {
        savePreference(Constant.DRIVER_ID, null);
        savePreference(Constant.DRIVER_NAME, null);
        savePreference(Constant.DRIVER_PHONE, null);
        savePreference(Constant.REFRESH_TOKEN, null);

        savePreference(Constant.DRIVER_NATIONAL_ID, null);
        savePreference(Constant.DRIVER_LICENSE, null);
        savePreference(Constant.DRIVER_VEHICLE_LICENSE, null);
        savePreference(Constant.DRIVER_LEGAL_CONSENT, null);
        savePreference(Constant.DRIVER_CONFIRMATION, null);

        savePreference(Constant.DRIVER_TOKEN, null);
        savePreference(Constant.DRIVER_DEVICE_ID, null);

        savePreference(Constant.DRIVER_CITY_ID, null);
        savePreference(Constant.DRIVER_CITY_NAME, null);
        savePreference(Constant.DRIVER_EMAIL, null);

        savePreference(Constant.DRIVER_REAL_TIME_TOKEN, null);

        savePreference(Constant.DRIVER_IMAGE, null);
        savePreference(Constant.DRIVER_NUMBER_OF_ORDERS, null);
        savePreference(Constant.DRIVER_APPROVED, false);
        savePreference(Constant.DRIVER_LOCAL_IMAGE, null);
        savePreference(UtilsFireBase.FIREBASE_TOKEN, null);
        savePreference(Constant.LAST_STATUS, null);


    }

    public ModelCaptain getCaptain() {
        ModelCaptain captain = new ModelCaptain();

        captain.setId(getPreference(Constant.DRIVER_ID, null));
        captain.setName(getPreference(Constant.DRIVER_NAME, null));
        captain.setPhone(getPreference(Constant.DRIVER_PHONE, null));
        captain.setRefreshToken(getPreference(Constant.REFRESH_TOKEN, null));


        captain.setToken(getPreference(Constant.DRIVER_TOKEN, null));
        captain.setDeviceID(getPreference(Constant.DRIVER_DEVICE_ID, null));
        captain.setEmail(getPreference(Constant.DRIVER_EMAIL, null));

        captain.setRealTimeToken(getPreference(Constant.DRIVER_REAL_TIME_TOKEN, null));

        captain.setImage(getPreference(Constant.DRIVER_IMAGE, null));
        captain.setNumberOFOrders(getPreference(Constant.DRIVER_NUMBER_OF_ORDERS, 0));
        captain.setApproved(getPreference(Constant.DRIVER_APPROVED, false));
        captain.setLocalImage(getPreference(Constant.DRIVER_LOCAL_IMAGE, null));

        ModelCity city = new ModelCity();
        city.setId(getPreference(Constant.DRIVER_CITY_ID, null));
        city.setName(getPreference(Constant.DRIVER_CITY_NAME, null));

        captain.setCity(city);

        return captain;
    }
*/

}
