package com.example.sicotask.utils;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

import com.example.sicotask.model.ProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.core.content.ContextCompat;

public class GlobalFunctions {


    public static boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static void setStatusBarColor(Activity context, int color) {
        if (context != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = context.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(context, color));
        }
    }

    public static List<ProductModel> populateDate() {
        List<ProductModel> productModels = new ArrayList<>();
        ProductModel model1 = new ProductModel("NileX");
        productModels.add(model1);
        ProductModel model2 = new ProductModel("NileY");
        productModels.add(model2);
        ProductModel model3 = new ProductModel("NileZ");
        productModels.add(model3);
        ProductModel model4 = new ProductModel("NileA");
        productModels.add(model4);
        ProductModel model5 = new ProductModel("NilB");
        productModels.add(model5);
        ProductModel model6 = new ProductModel("NileC");
        productModels.add(model6);
        ProductModel model7 = new ProductModel("NileB");
        productModels.add(model7);
        ProductModel model8 = new ProductModel("NileE");
        productModels.add(model8);
        ProductModel model9 = new ProductModel("NileF");
        productModels.add(model9);
        ProductModel model10 = new ProductModel("NileG");
        productModels.add(model10);
        return productModels;
    }
}
