package com.example.sicotask.utils;

public class Constant {
    public static final long CLICK_TIME_INTERVAL = 1000;
    public static final String SHOPPER = "123312";
    public static final String DATA_BASE_NAME = "sico.dp";
    public static final String LANGUAGE = "LANGUAGE";

    public static final String currency = "EGP";
    public static final String ApiKeyId = "23417421-a01e-4ab1-8cf5-6c1bde9f32eb";
    public static final String merchantId = "MID-641-914";

    public static final String SUCCESS = "SUCCESS";
}
